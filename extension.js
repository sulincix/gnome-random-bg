const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;

function init() {
    
}

function enable() {
    GLib.spawn_command_line_sync("rm -f /tmp/.unsplash.jpg",0);
    GLib.spawn_command_line_sync("curl -L https://source.unsplash.com/random/1920x1080 -o /tmp/.unsplash.jpg",0);
    GLib.spawn_command_line_sync("gsettings set org.gnome.desktop.background picture-uri /tmp/.unsplash.jpg",0);
}

function disable() {
    GLib.spawn_command_line_async("gsettings set org.gnome.desktop.background picture-uri /usr/share/backgrounds/gnome/adwaita-timed.xml", null);
}
